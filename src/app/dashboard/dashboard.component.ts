import { Component, OnInit } from '@angular/core';
import { Networkcall2Service } from '../servicefolder/networkcall2.service';
import { Sellerproduct } from '../model/sellerproduct';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  searchnomsg: string = '';
  success: boolean = false;
  nodatamsg: boolean = false;
  melamsg = '';
  productquan: number = 1;
  changeproduct = new Sellerproduct();
  addcartproduct = new Sellerproduct();
  productlist: Sellerproduct[] = [];
  constructor(
    private network2: Networkcall2Service,
    public router: Router,
    public activeroute: ActivatedRoute
  ) {}
  ngOnInit(): void {
    this.network2.populateproduct().subscribe((_data) => {
      if (_data) {
        this.productlist = _data;
        this.addcartproduct = _data;
      }
    });
    // this.activeroute.queryParams.subscribe((data) => {
    //   const message = data['message'];
    //   if (message) {
    //     this.searchnomsg = message;
    //     this.nodatamsg = true;
    //   }
    //   setTimeout(() => {
    //     this.nodatamsg = false;
    //   }, 2000);
    // });
  }
  godata(id: any) {
    this.router.navigate(['/productdetail'], { queryParams: { title: id } });
  }
  savedata(id: any) {
    this.network2.savelist(id).subscribe((data) => {
      if (data) {
        this.changeproduct = data;
        this.changeproduct.productquantity = this.productquan;
        this.network2.addcart(this.changeproduct).subscribe((_data) => {
          if (_data) {
            console.log(_data);
            this.melamsg = 'added';
            this.success = true;
          }
          setTimeout(() => {
            this.success = false;
          }, 2000);
        });
      }
    });
  }
}
