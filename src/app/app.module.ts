import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ModuleMaterialModule } from './module-material/module-material.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { SellerComponent } from './seller/seller.component';
import { CartComponent } from './cart/cart.component';
import { SellerHomeComponent } from './seller-home/seller-home.component';
import { SellerAddProductsComponent } from './seller-add-products/seller-add-products.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { OrderListComponent } from './order-list/order-list.component';
import { BuyNowComponent } from './buy-now/buy-now.component';
import { SerachComponentComponent } from './serach-component/serach-component.component';
import { OrdertickComponent } from './ordertick/ordertick.component';
import { SearchBuyComponent } from './search-buy/search-buy.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    SellerComponent,
    CartComponent,
    SellerHomeComponent,
    SellerAddProductsComponent,
    ProductDetailsComponent,
    CheckOutComponent,
    OrderListComponent,
    BuyNowComponent,
    SerachComponentComponent,
    OrdertickComponent,
    SearchBuyComponent,
  ],
  imports: [
    AppRoutingModule,
    ModuleMaterialModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule, // Required for animations
    ToastrModule.forRoot({
      timeOut: 3000, // Toast message duration in milliseconds
      positionClass: 'toast-top-right', // Toast position
      preventDuplicates: true, // Prevent duplicate toasts
    }),
  ],

  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
