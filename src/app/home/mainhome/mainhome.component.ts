import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-mainhome',
  templateUrl: './mainhome.component.html',
  styleUrls: ['./mainhome.component.css'],
})
export class MainhomeComponent implements OnInit {
  searchnomsg: string = '';
  nodatamsg: boolean = false;
  menutype = 'default';
  loginname = '';
  constructor(public router: Router, public activeroute: ActivatedRoute) {}
  ngOnInit(): void {
    // this.router.events.subscribe((val: any) => {
    //   if (val.url) {
    //     if (localStorage.getItem('Seller') && val.url.includes('Seller')) {
    //       const a = localStorage.getItem('Seller');
    //       if (a) {
    //         const b = JSON.parse(a);
    //         this.loginname = b.username;
    //       }
    //       this.menutype = 'seller';
    //     } else {
    //       this.menutype = 'default';
    //     }
    //   }
    // });
    this.activeroute.queryParams.subscribe((data) => {
      const message = data['message'];
      if (message) {
        this.searchnomsg = message;
        this.nodatamsg = true;
      }
      setTimeout(() => {
        this.nodatamsg = false;
      }, 2000);
    });
  }
  logout() {
    localStorage.removeItem('Seller');
    this.router.navigate(['']);
  }
  submit(productname: string) {
    this.router.navigate(['/search'], { queryParams: { title: productname } });
  }
}
