import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { MainhomeComponent } from './mainhome/mainhome.component';
import { ModuleMaterialModule } from '../module-material/module-material.module';
import { HttpClientModule } from '@angular/common/http';
import { SellerComponent } from '../seller/seller.component';

@NgModule({
  declarations: [MainhomeComponent],
  imports: [CommonModule, HomeRoutingModule, ModuleMaterialModule],
})
export class HomeModule {}
