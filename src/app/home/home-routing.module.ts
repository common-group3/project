import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { MainhomeComponent } from './mainhome/mainhome.component';
import { CartComponent } from '../cart/cart.component';

const routes: Routes = [
  {
    path: '',
    component: MainhomeComponent,
    children: [
      {
        path: '',
        component: DashboardComponent,
      },
      {
        path: 'Cart',
        component: CartComponent,
      },
    ],
  },
  { path: 'Cart', component: CartComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
