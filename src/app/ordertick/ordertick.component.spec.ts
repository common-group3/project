import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdertickComponent } from './ordertick.component';

describe('OrdertickComponent', () => {
  let component: OrdertickComponent;
  let fixture: ComponentFixture<OrdertickComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrdertickComponent]
    });
    fixture = TestBed.createComponent(OrdertickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
