import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Sellerproduct } from '../model/sellerproduct';
import { Observable } from 'rxjs';
import { Checkout } from '../model/checkout';
import { Registeration } from '../model/Registeration';
@Injectable({
  providedIn: 'root',
})
export class Networkcall2Service {
  constructor(private httpclient: HttpClient) {}

  public URLRegisteration2 = 'http://localhost:8080/student/addcart';
  public URLRegisteration = 'http://localhost:8080/student/selleradd';
  public URLRegisteration1 = 'http://localhost:8080/student/selleradd';
  public URLRegisteration3 = 'http://localhost:8080/student/orderlist';
  public URLRegisteration4 = 'http://localhost:8080/student/orderget';
  public URLRegisteration5 = 'http://localhost:8080/student/deleteall';

  public addproducts(selpro: Sellerproduct): Observable<Object> {
    return this.httpclient.post(this.URLRegisteration, selpro);
  }

  public addcart(selpro: Sellerproduct): Observable<Object> {
    return this.httpclient.post(this.URLRegisteration2, selpro);
  }
  // placeadd tha na appo suma placelist nu kuduthuta..
  public placelist(check: Checkout): Observable<Object> {
    return this.httpclient.post(this.URLRegisteration3, check);
  }
  // buy now ordertable kaga separate ta pannathu..
  public buynow(selpro: Sellerproduct): Observable<Object> {
    return this.httpclient.post('http://localhost:8080/student/buynow', selpro);
  }
  public buyall() {
    return this.httpclient.get<any>('http://localhost:8080/student/buyall');
  }
  public placeget() {
    return this.httpclient.get<any>(this.URLRegisteration4);
  }

  public deleteall() {
    return this.httpclient.get<any>(this.URLRegisteration5);
  }

  public deletenow() {
    return this.httpclient.get<any>('http://localhost:8080/student/deletenow');
  }

  public productlist() {
    return this.httpclient.get<any>(this.URLRegisteration);
  }

  public productlist2() {
    return this.httpclient.get<any>(this.URLRegisteration2);
  }
  public savelist(id: number) {
    return this.httpclient.get(
      'http://localhost:8080/student/getunique' + '?id=' + id
    );
  }

  public deleteproduct(_id: number) {
    return this.httpclient.get(
      'http://localhost:8080/student/sellerdelete' + '?id=' + _id
    );
  }

  public deleteorder(id: number) {
    return this.httpclient.get(
      'http://localhost:8080/student/orderdelete' + '?id=' + id
    );
  }
  public deleteproduct1(id: number) {
    return this.httpclient.get(
      'http://localhost:8080/student/cartdelete' + '?id=' + id
    );
  }
  public populateproduct() {
    return this.httpclient.get<any>('http://localhost:8080/student/home');
  }

  public prorecord(id: any) {
    return this.httpclient.get<any>(
      'http://localhost:8080/student/record' + '?id=' + id
    );
  }

  public prorecord2(proname: any) {
    return this.httpclient.get<any>(
      'http://localhost:8080/student/record1' + '?proname=' + proname
    );
  }

  public addrecord(id: any) {
    return this.httpclient.get<any>(
      'http://localhost:8080/student/record' + '?id=' + id
    );
  }

  public useredit(str: any) {
    return this.httpclient.get<any>(
      'http://localhost:8080/student/useredit' + '?str=' + str
    );
  }
  public update(regi: Registeration) {
    return this.httpclient.put<Registeration>(
      `http://localhost:8080/student/editdone/${regi.id}`,
      regi
    );
  }
}
