import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class Formvalidation2Service {
  constructor() {}
  userforms: FormGroup = new FormGroup({
    productnumber: new FormControl('', Validators.required),
    productname: new FormControl('', Validators.required),
    productrole: new FormControl('', Validators.required),
    productcolour: new FormControl('', Validators.required),
    productdescription: new FormControl('', Validators.required),
    productimageurl: new FormControl('', Validators.required),
  });
}
