import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class FormvalidationService {
  constructor() {}
  userforms: FormGroup = new FormGroup({
    username: new FormControl('', Validators.required),
    mothername: new FormControl('', Validators.required),
    fathername: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    userid: new FormControl('', Validators.required),
  });
}
