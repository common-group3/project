import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Registeration } from '../model/Registeration';
@Injectable({
  providedIn: 'root',
})
export class UserService {
  private URLRegisteration = 'http://localhost:8080/student/login';

  constructor(private http: HttpClient) {}

  loginuser(regi: Registeration) {
    return this.http.post(this.URLRegisteration, regi);
  }
}
