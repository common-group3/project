import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Registeration } from '../model/Registeration';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NetworkcallService {
  public URLRegisteration = 'http://localhost:8080/student/registeration';

  constructor(private httpclient: HttpClient) {}

  createstudentregisteration(studentregi: Registeration): Observable<Object> {
    return this.httpclient.post(this.URLRegisteration, studentregi);
  }
}
