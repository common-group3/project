import { Component, OnInit } from '@angular/core';
import { Checkout } from '../model/checkout';
import { CheckserviceService } from '../servicefolder/checkservice.service';
import { Networkcall2Service } from '../servicefolder/networkcall2.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.css'],
})
export class CheckOutComponent implements OnInit {
  success: boolean = false;
  totalprice: number | undefined;
  notimsg: string | undefined;
  check = new Checkout();
  constructor(
    public checkservice: CheckserviceService,
    public network2: Networkcall2Service,
    public router: Router
  ) {
    this.network2.productlist2().subscribe((data) => {
      let price = 0;
      data.forEach((item: any) => {
        if (item.productquantity) {
          price = price + +item.productprice * +item.productquantity;
        }
      });
      this.totalprice = price + price / 10 + 100 - price / 10;
    });
  }
  ngOnInit(): void {}
  ordernow() {
    let user = localStorage.getItem('Seller');
    let username = user && JSON.parse(user).username;
    if (this.totalprice) {
      this.check.username = username;
      this.check.amount = this.totalprice;
      this.network2.placelist(this.check).subscribe((data) => {
        if (data) {
          this.notimsg = 'ordered placed';
          this.success = true;
        }
      });
    }
    setTimeout(() => {
      this.success = false;
      this.notimsg = undefined;
      this.router.navigate(['/orderlist']);
    }, 2000);
  }
}
