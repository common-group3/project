import { Component } from '@angular/core';
import { FormvalidationService } from '../servicefolder/formvalidation.service';
import { NetworkcallService } from '../servicefolder/networkcall.service';
import { Registeration } from '../model/Registeration';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent {
  jumb: boolean = true;
  userregimodel: Registeration = new Registeration();
  constructor(
    public formvalidationservice: FormvalidationService,
    public urlservice: NetworkcallService,
    public dialogReference: MatDialogRef<RegisterComponent>,
    public router: Router
  ) {}
  ngOninit(): void {}
  submitdata() {
    this.urlservice
      .createstudentregisteration(this.userregimodel)
      .subscribe((_data) => {});
    this.Onclose();
    if (this.jumb === true) {
      this.router.navigate(['']);
    }
  }
  Onclose() {
    this.dialogReference.close();
    console.log('formvalidationservice.userforms.invalid');
  }
}
