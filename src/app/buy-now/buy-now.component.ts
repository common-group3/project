import { Component } from '@angular/core';
import { Checkout } from '../model/checkout';
import { CheckserviceService } from '../servicefolder/checkservice.service';
import { OnInit } from '@angular/core';
import { Networkcall2Service } from '../servicefolder/networkcall2.service';
import { Sellerproduct } from '../model/sellerproduct';
import { Router } from '@angular/router';

@Component({
  selector: 'app-buy-now',
  templateUrl: './buy-now.component.html',
  styleUrls: ['./buy-now.component.css'],
})
export class BuyNowComponent implements OnInit {
  success: boolean = false;
  totalprice: number | undefined;
  check = new Checkout();
  product: Sellerproduct[] = [];
  notimsg: undefined | string;

  constructor(
    public checkservice: CheckserviceService,
    public network2: Networkcall2Service,
    public router: Router
  ) {
    let user = localStorage.getItem('Seller');
    let username = user && JSON.parse(user).username;
    let price = 0;
    this.network2.buyall().subscribe((data) => {
      data.forEach((item: any) => {
        if (item.productquantity) {
          price = price + +item.productprice * +item.productquantity;
        }
      });
      this.totalprice = price + price / 10 + 100 - price / 10;
      this.check.amount = this.totalprice;
      this.check.username = username;
    });
  }

  ngOnInit(): void {
    this.network2.deletenow().subscribe((data) => {
      if (data) {
      }
    });
  }

  ordernow() {
    this.network2.placelist(this.check).subscribe((data) => {
      if (data) {
        this.notimsg = 'ordered placed';
        this.success = true;
      }
      setTimeout(() => {
        this.notimsg = undefined;
        this.success = false;
        this.router.navigate(['/orderlist']);
      }, 4000);
    });
  }
}
