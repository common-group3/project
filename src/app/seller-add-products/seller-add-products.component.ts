import { Component, OnInit } from '@angular/core';
import { Sellerproduct } from '../model/sellerproduct';
import { Networkcall2Service } from '../servicefolder/networkcall2.service';
import { Formvalidation2Service } from '../servicefolder/formvalidation2.service';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-seller-add-products',
  templateUrl: './seller-add-products.component.html',
  styleUrls: ['./seller-add-products.component.css'],
})
export class SellerAddProductsComponent implements OnInit {
  success: boolean = false;
  succmsg: string | undefined;
  selpro = new Sellerproduct();
  constructor(
    private network2: Networkcall2Service,
    public formvalidationservice: Formvalidation2Service,
    private fb: FormBuilder
  ) {}
  form: FormGroup = this.fb.group({
    productnumber: [''],
    productprice: [''],
    productrole: [''],
    productcolour: [''],
    productdescription: [''],
    productimageurl: [''],
  });
  ngOnInit(): void {}
  getdata() {
    this.network2.addproducts(this.selpro).subscribe((_data) => {
      if (_data) {
        this.succmsg = 'Product added Successfully';
        this.success = true;
        this.form.reset();
        this.resetForm();
        console.log('reset');
      }
    });
    setTimeout(() => {
      this.succmsg = undefined;
      this.success = false;
    }, 3000);
  }
  resetForm() {
    const controls = this.form.controls;
    Object.keys(controls).forEach((controlName) => {
      controls[controlName].reset('');
    });
  }
}
