import { Component } from '@angular/core';
import { UserService } from '../servicefolder/user.service';
import { Registeration } from '../model/Registeration';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterComponent } from '../register/register.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  jumb: boolean = true;
  success: boolean = false;
  alert: boolean = false;
  resmsg!: string;
  regi = new Registeration();
  username2 = '';
  password2 = '';
  constructor(
    private loginservice: UserService,
    private fb: FormBuilder,
    public router: Router,
    private dialog: MatDialog
  ) {}

  form: FormGroup = this.fb.group({
    username: [''],
    password: [''],
  });
  getdata() {
    this.loginservice.loginuser(this.regi).subscribe((data: any) => {
      if (data) {
        this.resmsg = data.responceMsg;
        if (this.resmsg === 'LoginSuccessfully') {
          // this.username2 = this.regi.username ? this.regi.username : '';
          localStorage.setItem('Seller', JSON.stringify(this.regi));
          // sessionStorage.setItem('Seller', this.username2);
          // this.password2 = this.regi.password ? this.regi.password : '';
          // localStorage.setItem('Seller', this.password2);
          // sessionStorage.setItem('Seller', this.password2);
          if (this.jumb === true) {
            this.success = true;
            setTimeout(() => {
              this.router.navigate(['/homemain']);
            }, 1000);
          }
        } else {
          this.alert = true;
          this.form.reset();
          setTimeout(() => {
            this.alert = false;
          }, 2000);
        }
      }
    });
  }
  closealert1() {
    this.success = false;
  }
  closealert2() {
    this.alert = false;
  }
  method1() {
    this.dialog.open(RegisterComponent, {
      width: '90%',
      height: '60%',
    });
  }
}
