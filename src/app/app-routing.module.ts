import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
//import { RegisterComponent } from './register/register.component';
import { CartComponent } from './cart/cart.component';
import { SellerComponent } from './seller/seller.component';
import { SellerHomeComponent } from './seller-home/seller-home.component';
import { SellerAddProductsComponent } from './seller-add-products/seller-add-products.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { OrderListComponent } from './order-list/order-list.component';
import { BuyNowComponent } from './buy-now/buy-now.component';
import { SerachComponentComponent } from './serach-component/serach-component.component';
import { SearchBuyComponent } from './search-buy/search-buy.component';

const routes: Routes = [
  {
    path: 'Dash',
    component: DashboardComponent,
    pathMatch: 'full',
  },
  {
    path: '',
    component: LoginComponent,
    pathMatch: 'full',
  },
  {
    path: 'Cart',
    component: CartComponent,
    pathMatch: 'full',
  },
  {
    path: 'Seller',
    component: SellerComponent,
    pathMatch: 'full',
  },
  {
    path: 'Sellerhome',
    component: SellerHomeComponent,
    pathMatch: 'full',
  },
  {
    path: 'Selleradd',
    component: SellerAddProductsComponent,
    pathMatch: 'full',
  },
  {
    path: 'productdetail',
    component: ProductDetailsComponent,
    pathMatch: 'full',
  },
  {
    path: 'checkout',
    component: CheckOutComponent,
    pathMatch: 'full',
  },
  {
    path: 'orderlist',
    component: OrderListComponent,
    pathMatch: 'full',
  },
  {
    path: 'buynow',
    component: BuyNowComponent,
    pathMatch: 'full',
  },
  {
    path: 'search',
    component: SerachComponentComponent,
    pathMatch: 'full',
  },
  {
    path: 'searchbuy',
    component: SearchBuyComponent,
    pathMatch: 'full',
  },
  {
    path: 'homemain',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
