import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchBuyComponent } from './search-buy.component';

describe('SearchBuyComponent', () => {
  let component: SearchBuyComponent;
  let fixture: ComponentFixture<SearchBuyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SearchBuyComponent]
    });
    fixture = TestBed.createComponent(SearchBuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
