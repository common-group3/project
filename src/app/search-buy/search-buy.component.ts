import { Component, OnInit } from '@angular/core';
import { Sellerproduct } from '../model/sellerproduct';
import { Checkout } from '../model/checkout';
import { CheckserviceService } from '../servicefolder/checkservice.service';
import { Router } from '@angular/router';
import { Networkcall2Service } from '../servicefolder/networkcall2.service';

@Component({
  selector: 'app-search-buy',
  templateUrl: './search-buy.component.html',
  styleUrls: ['./search-buy.component.css'],
})
export class SearchBuyComponent implements OnInit {
  success: boolean = false;
  totalprice: number | undefined;
  check = new Checkout();
  product: Sellerproduct[] = [];
  notimsg: undefined | string;
  constructor(
    public checkservice: CheckserviceService,
    public network2: Networkcall2Service,
    public router: Router
  ) {
    let price = 0;
    let user = localStorage.getItem('Seller');
    let username = user && JSON.parse(user).username;
    this.network2.buyall().subscribe((data) => {
      data.forEach((item: any) => {
        if (item.productquantity) {
          price = price + +item.productprice * +item.productquantity;
          this.totalprice = price + price / 10 + 100 - price / 10;
          this.check.amount = this.totalprice;
          this.check.username = username;
        }
      });
    });
  }
  ngOnInit(): void {
    this.network2.deletenow().subscribe((data) => {
      if (data) {
        console.log('error');
      }
    });
  }
  ordernow() {
    this.network2.placelist(this.check).subscribe((data) => {
      if (data) {
        this.notimsg = 'ordered placed';
        this.success = true;
      }
      setTimeout(() => {
        this.notimsg = undefined;
        this.success = false;
        this.router.navigate(['/orderlist']);
      }, 4000);
    });
  }
}
