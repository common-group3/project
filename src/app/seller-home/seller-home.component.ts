import { Component, OnInit } from '@angular/core';
import { Networkcall2Service } from '../servicefolder/networkcall2.service';
import { Sellerproduct } from '../model/sellerproduct';

@Component({
  selector: 'app-seller-home',
  templateUrl: './seller-home.component.html',
  styleUrls: ['./seller-home.component.css'],
})
export class SellerHomeComponent implements OnInit {
  success: boolean = false;
  notimsg: undefined | string;
  productlist: Sellerproduct[] = [];
  displayedColumns: string[] = [
    'position',
    'name',
    'weight',
    'symbol',
    'hello',
    'hi',
    'ok',
  ];
  constructor(public network2: Networkcall2Service) {}
  ngOnInit(): void {
    this.network2.productlist().subscribe((_data) => {
      if (_data) {
        this.productlist = _data;
      }
    });
  }
  dataSource = this.productlist;

  deleteproduct(id: number) {
    this.network2.deleteproduct(id).subscribe((_data) => {
      if (_data) {
        this.notimsg = 'Succesfully Deleted';
        this.network2.productlist().subscribe((_data) => {
          this.productlist = _data;
          this.success = true;
        });
        setTimeout(() => {
          this.notimsg = undefined;
          this.success = false;
        }, 3000);
      }
    });
  }
}
