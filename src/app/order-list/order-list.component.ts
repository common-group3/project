import { Component, OnInit } from '@angular/core';
import { Networkcall2Service } from '../servicefolder/networkcall2.service';
import { Checkout } from '../model/checkout';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css'],
})
export class OrderListComponent implements OnInit {
  success: boolean = false;
  check: Checkout[] = [];
  constructor(public network2: Networkcall2Service) {}
  displayedColumns: string[] = [
    'hi',
    'position',
    'hello',
    'name',
    'weight',
    'symbol',
    'pk',
    'ok',
  ];
  dataSource = this.check;
  ngOnInit(): void {
    this.network2.placeget().subscribe((data) => {
      if (data) {
        this.check = data;
      }
    });
  }
  deleteproduct(id: any) {
    this.network2.deleteorder(id).subscribe((data) => {
      this.success = true;
    });
    setTimeout(() => {
      this.success = false;
      this.ngOnInit();
    }, 2000);
  }
}
