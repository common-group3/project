import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Networkcall2Service } from '../servicefolder/networkcall2.service';
import { Sellerproduct } from '../model/sellerproduct';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
})
export class ProductDetailsComponent {
  isInputDisabled: boolean = false;

  success: boolean = false;
  commentmsg: undefined | string;
  boxmsg: undefined | string;
  messg: undefined | string;
  productquan: number = 1;
  product = new Sellerproduct();
  buynowproduct = new Sellerproduct();
  productid: any;
  constructor(
    private activeroute: ActivatedRoute,
    private newtwork2: Networkcall2Service,
    public router: Router
  ) {
    this.activeroute.queryParams.subscribe((data) => {
      const identityValue = data['title'];

      if (identityValue != null) {
        this.productid = identityValue;
        this.newtwork2.prorecord(this.productid).subscribe((_data) => {
          this.product = _data;
        });
      }
    });
  }
  handlequan(val: string) {
    if (this.productquan < 20 && val === 'plus') {
      this.productquan += 1;
    } else if (this.productquan > 1 && val === 'min') {
      this.productquan -= 1;
    }
  }
  database() {
    this.product.productquantity = this.productquan;
    this.newtwork2.addcart(this.product).subscribe((_data) => {
      if (_data) {
        this.messg = 'product Added to Cart';
        this.success = true;
      }
    });
    setTimeout(() => {
      this.messg = undefined;
      this.success = false;
    }, 2000);
  }
  changing() {
    this.boxmsg = this.commentmsg;
    // Disable the input box after submission
    this.isInputDisabled = true;
    this.commentmsg = '';
  }
  anotherroute(id: any) {
    this.newtwork2.savelist(id).subscribe((data) => {
      if (data) {
        this.buynowproduct = data;
        this.buynowproduct.productquantity = this.productquan;
        this.newtwork2.buynow(this.buynowproduct).subscribe((data) => {
          if (data) {
          }
        });
      }
    });
    this.router.navigate(['buynow']);
  }
}
