export interface pricesummary {
  price: number;
  discount: number;
  tax: number;
  delivery: number;
  total: number;
}
