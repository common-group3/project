export class Cart {
  id?: number;
  productprice?: number;
  productname?: string;
  productrole?: string;
  productcolour?: string;
  productdescription?: string;
  productimageurl?: string;
}
