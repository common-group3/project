export class Checkout {
  email?: string;
  address?: string;
  contact?: number;
  username?: string;
  amount?: number;
  id?: number;
}
