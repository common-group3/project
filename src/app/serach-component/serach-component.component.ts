import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Networkcall2Service } from '../servicefolder/networkcall2.service';
import { Sellerproduct } from '../model/sellerproduct';
import { Router } from '@angular/router';

@Component({
  selector: 'app-serach-component',
  templateUrl: './serach-component.component.html',
  styleUrls: ['./serach-component.component.css'],
})
export class SerachComponentComponent implements OnInit {
  product = new Sellerproduct();
  buynowproduct = new Sellerproduct();
  productquan: number = 1;
  proname: string = '';
  constructor(
    public activeroute: ActivatedRoute,
    public network2: Networkcall2Service,
    public router: Router
  ) {}
  ngOnInit(): void {
    this.activeroute.queryParams.subscribe((data) => {
      const identityValue = data['title'];
      if (identityValue != null) {
        this.proname = identityValue;
        this.network2.prorecord2(this.proname).subscribe((data) => {
          if (data != null) {
            this.product = data;
          } else {
            this.router.navigate(['homemain'], {
              queryParams: { message: 'No data found' },
            });
          }
        });
      }
    });
  }
  searchbuy(id: any) {
    this.network2.savelist(id).subscribe((data) => {
      if (data) {
        this.buynowproduct = data;
        this.buynowproduct.productquantity = this.productquan;
        this.network2.buynow(this.buynowproduct).subscribe((data) => {
          if (data) {
          }
        });
      }
    });
    this.router.navigate(['searchbuy']);
  }
}
