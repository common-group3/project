import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Networkcall2Service } from '../servicefolder/networkcall2.service';
import { Registeration } from '../model/Registeration';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-seller',
  templateUrl: './seller.component.html',
  styleUrls: ['./seller.component.css'],
})
export class SellerComponent implements OnInit {
  regi = new Registeration();
  menutype = 'default';
  updatemessage = '';
  loginname = '';
  password = '';
  regiForm: FormGroup;
  constructor(
    private router: Router,
    public network2: Networkcall2Service,
    private formBuilder: FormBuilder
  ) {
    this.regiForm = this.formBuilder.group({
      username: '',
      userid: '',
      address: '',
      password: '',
      fathername: '',
      mothername: '',
      id: '',
    });
  }
  ngOnInit(): void {
    // this.router.events.subscribe((val: any) => {
    //   if (val.url) {
    //     if (localStorage.getItem('Seller') && val.url.includes('Seller')) {
    const a = localStorage.getItem('Seller');
    if (a) {
      const b = JSON.parse(a);
      this.loginname = b.username;
      this.password = b.password;
    }
    //   }
    // }
    //   });
    // }
  }
  logout() {
    localStorage.removeItem('Seller');
    this.router.navigate(['']);
  }
  editProfile(str: any) {
    this.network2.useredit(str).subscribe((data) => {
      if (data) {
        this.regi = data;
      }
    });
  }
  getdata(regi: any) {
    this.network2.update(regi).subscribe((data) => {
      if (data) {
        this.updatemessage = 'Product Updated..';
        console.log(this.updatemessage);
        this.regiForm.reset();
      }
    });
  }
}
