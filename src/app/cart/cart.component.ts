import { Component, OnInit } from '@angular/core';
import { Networkcall2Service } from '../servicefolder/networkcall2.service';
import { Sellerproduct } from '../model/sellerproduct';
import { pricesummary } from '../model/pricesummary';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  isButtonDisabled: boolean = true;
  productlist: Sellerproduct[] = [];
  pricesummary: pricesummary = {
    price: 0,
    discount: 0,
    tax: 0,
    delivery: 0,
    total: 0,
  };
  constructor(private network2: Networkcall2Service, private router: Router) {}
  ngOnInit(): void {
    this.network2.productlist2().subscribe((data) => {
      if (data.length === 0) {
        this.isButtonDisabled = true;
      } else {
        this.isButtonDisabled = false;
      }

      this.productlist = data;

      let price = 0;
      data.forEach((item: any) => {
        if (item.productquantity) {
          price = price + +item.productprice * +item.productquantity;
        } else {
          price = price + +item.productprice * +1;
        }
      });
      this.pricesummary.price = price;
      this.pricesummary.discount = price / 10;
      this.pricesummary.tax = price / 10;
      this.pricesummary.delivery = 100;
      this.pricesummary.total = price + price / 10 + 100 - price / 10;
    });
  }
  delete(id: any) {
    this.network2.deleteproduct1(id).subscribe((_data) => {
      if (_data) {
        this.ngOnInit();
      }
    });
  }
  checkout() {
    this.router.navigate(['checkout']);
    this.network2.deleteall().subscribe((data) => {
      if (data) {
      }
    });
  }
}
